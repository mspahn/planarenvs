import numpy as np
from numpy import sin, cos, pi
import time

from scipy.integrate import odeint

from gym import core, spaces
from gym.utils import seeding


class PointRobotVelEnv(core.Env):

    metadata = {"render.modes": ["human", "rgb_array"], "video.frames_per_second": 15}

    MAX_VEL = 10
    MAX_POS = 10

    def __init__(self, n=2, dt=0.01, render=False):
        self._n = n
        self.viewer = None
        limUpPos = [self.MAX_POS for i in range(n)]
        limUpVel = [self.MAX_VEL for i in range(n)]
        high = np.array(limUpPos,  dtype=np.float32)
        low = -high
        self.observation_space = spaces.Box(low=low, high=high, dtype=np.float64)
        self.action_space = spaces.Box(
            low=-np.array(limUpVel), high=np.array(limUpVel), dtype=np.float64
        )
        self.state = None
        self.seed()
        self._dt = dt
        self._render = render

    def dt(self):
        return self._dt

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def reset(self, initPos, initVel):
        self.state = np.concatenate((initPos, initVel))
        return self._get_ob()

    def step(self, a):
        s = self.state
        self.action = a
        ns = self.integrate()
        self.state = np.concatenate((ns, a))
        terminal = self._terminal()
        reward = -1.0 if not terminal else 0.0
        if self._render:
            self.render()
        return (self._get_ob(), reward, terminal, {})

    def _get_ob(self):
        return self.state

    def _terminal(self):
        s = self.state
        return False

    def continuous_dynamics(self, x, t):
        return self.action

    def integrate(self):
        x0 = self.state[0:self._n]
        t = np.arange(0, 2 * self._dt, self._dt)
        vel = self.continuous_dynamics(x0, t)
        ynext = x0 + self._dt * vel
        return ynext

    def render(self, mode="human"):
        from gym.envs.classic_control import rendering

        s = self.state
        if s is None:
            return None

        bound = 5.0
        if self.viewer is None:
            self.viewer = rendering.Viewer(500, 500)
            self.viewer.set_bounds(-bound, bound, -bound, bound)

        self.viewer.draw_line((-bound, 0), (bound, 0))
        self.viewer.draw_line((0, -bound), (0, bound))
        x = s[0]
        y = 0.0
        if self._n == 2:
            y = s[1]
        tf0 = rendering.Transform(rotation=0, translation=(x, y))
        joint = self.viewer.draw_circle(.10)
        joint.set_color(.8, .8, 0)
        joint.add_attr(tf0)
        time.sleep(self.dt())

        return self.viewer.render(return_rgb_array=mode == "rgb_array")

    def close(self):
        if self.viewer:
            self.viewer.close()
            self.viewer = None
